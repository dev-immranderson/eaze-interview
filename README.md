- This makes heavy usage of a custom recycler adapter pattern that I enjoy using.
Typically if you wish to create a recycler adapter that has multiple types and can have variable length,
your adapter code can get rather bloated and hard to manage. I abstract a lot of the pain that goes into
having a complex recycleradapter into the BaseStateModel, BaseViewHolder, and AbstractBaseRecyclerAdapter classes.
These all interact together to manage adapter sizing and multiple viewholder types far less painful.

- I also use databinding exclusively to get rid of findViewById calls. This also eliminates the need for
other dependencies like ButterKnife, which in comparison, is a bit more wordy than using databinding.
Each class accesses the views within the databinding through their variable b after DataBindingUtils
has attached it to the parent layout/view.