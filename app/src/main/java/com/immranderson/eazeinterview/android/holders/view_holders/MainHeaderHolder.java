package com.immranderson.eazeinterview.android.holders.view_holders;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.immranderson.eazeinterview.android.databinding.HolderHeaderTextBinding;
import com.immranderson.eazeinterview.android.holders.state_models.MainHeaderState;

public class MainHeaderHolder extends BaseViewHolder<MainHeaderState> {

    private HolderHeaderTextBinding b;
    public MainHeaderHolder(View itemView) {
        super(itemView);
        b = DataBindingUtil.bind(itemView);
    }

    @Override
    public void bindData(MainHeaderState mainHeaderState) {

        b.headerTextView.setText(mainHeaderState.headerText);

    }
}
