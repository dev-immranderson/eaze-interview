package com.immranderson.eazeinterview.android.holders.state_models;

import android.view.ViewGroup;

import com.immranderson.eazeinterview.android.R;
import com.immranderson.eazeinterview.android.holders.HolderTypes;
import com.immranderson.eazeinterview.android.holders.view_holders.BaseViewHolder;
import com.immranderson.eazeinterview.android.holders.view_holders.MainPageGifCardHolder;
import com.immranderson.eazeinterview.android.models.GifModel;

public class MainGifCardHolderState extends BaseStateModel {

    public GifModel gifModel;

    public MainGifCardHolderState(GifModel gifModel) {
        this.gifModel = gifModel;
    }

    @Override
    public int getType() {
        return HolderTypes.MAIN_GIF_CARD;
    }

    @Override
    public int getViewHolderResId() {
        return R.layout.holder_main_page_gif;
    }

    @Override
    public BaseViewHolder createViewHolder(ViewGroup parent) {
        return new MainPageGifCardHolder(inflateFrom(parent));
    }
}
