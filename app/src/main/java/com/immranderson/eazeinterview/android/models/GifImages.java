package com.immranderson.eazeinterview.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GifImages {

    @Expose
    @SerializedName("downsized")
    public GifDownsized gifDownsized;

//    "downsized": {
//        "url": "https://media0.giphy.com/media/gl8ymnpv4Sqha/giphy-tumblr.gif",
//                "width": "250",
//                "height": "210",
//                "size": "1268794"
//    }

}
