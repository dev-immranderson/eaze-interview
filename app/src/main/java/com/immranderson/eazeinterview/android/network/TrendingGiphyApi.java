package com.immranderson.eazeinterview.android.network;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TrendingGiphyApi {

    @GET("trending")
    Observable<Response<JsonObject>> getTrendingGifs(@Query("api_key") String apiKey);

}
