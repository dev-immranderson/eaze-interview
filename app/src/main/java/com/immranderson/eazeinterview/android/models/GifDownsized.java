package com.immranderson.eazeinterview.android.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GifDownsized {

    @Expose
    @SerializedName("url")
    public String url;
}
