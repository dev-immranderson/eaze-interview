package com.immranderson.eazeinterview.android.holders.state_models;

import android.view.ViewGroup;

import com.immranderson.eazeinterview.android.R;
import com.immranderson.eazeinterview.android.adapters.BaseRecyclerAdapter;
import com.immranderson.eazeinterview.android.holders.HolderTypes;
import com.immranderson.eazeinterview.android.holders.view_holders.BaseViewHolder;
import com.immranderson.eazeinterview.android.holders.view_holders.RecyclerHolder;

public class RecyclerStateModel extends BaseStateModel {

    public final BaseRecyclerAdapter baseRecyclerAdapter;

    public RecyclerStateModel(BaseRecyclerAdapter baseRecyclerAdapter) {
        this.baseRecyclerAdapter = baseRecyclerAdapter;
    }

    @Override
    public int getType() {
        return HolderTypes.RECYCLER_200dp;
    }

    @Override
    public int getViewHolderResId() {
        return R.layout.holder_recycler_200dp;
    }

    @Override
    public BaseViewHolder createViewHolder(ViewGroup parent) {
        return new RecyclerHolder(inflateFrom(parent));
    }

}
