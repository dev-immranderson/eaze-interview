package com.immranderson.eazeinterview.android.holders.view_holders;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.bumptech.glide.Glide;
import com.immranderson.eazeinterview.android.databinding.HolderMainPageGifBinding;
import com.immranderson.eazeinterview.android.holders.state_models.MainGifCardHolderState;

public class MainPageGifCardHolder extends BaseViewHolder<MainGifCardHolderState> {

    private HolderMainPageGifBinding b;

    public MainPageGifCardHolder(View itemView) {
        super(itemView);
        b = DataBindingUtil.bind(itemView);
    }

    @Override
    public void bindData(MainGifCardHolderState mainGifCardHolderState) {

        Glide.with(itemView.getContext())
                .asGif()
                .load(mainGifCardHolderState.gifModel.images.gifDownsized.url)
                .into(b.mainGifImageView);

    }

}
