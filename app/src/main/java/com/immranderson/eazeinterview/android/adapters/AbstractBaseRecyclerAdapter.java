package com.immranderson.eazeinterview.android.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.immranderson.eazeinterview.android.holders.state_models.BaseStateModel;
import com.immranderson.eazeinterview.android.holders.view_holders.BaseViewHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractBaseRecyclerAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<BaseStateModel> pHolderDataList = new ArrayList<>();
    private Map<Integer, BaseStateModel> holderDataRef = new HashMap<>();


    public void addViewData(@NonNull BaseStateModel holderData) {

        pHolderDataList.add(holderData);
        holderDataRef.put(holderData.getType(), holderData);

    }

    public void addViewDataCollection(@NonNull Collection<? extends BaseStateModel> holderData) {

        for (BaseStateModel vhd : holderData) {

            pHolderDataList.add(vhd);
            holderDataRef.put(vhd.getType(), vhd);

        }

    }

    public void addViewDataAtPosition(int position, @NonNull BaseStateModel holderData) {

        pHolderDataList.add(position, holderData);
        holderDataRef.put(holderData.getType(), holderData);

    }

    public List<BaseStateModel> getViewDataList() {

        return pHolderDataList;

    }

    @Override
    public int getItemViewType(int position) {

        return pHolderDataList.get(position).getType();

    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return holderDataRef.get(viewType).createViewHolder(parent);

    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        holder.bindData(pHolderDataList.get(position));

    }

    @Override
    public int getItemCount() {

        return pHolderDataList.size();

    }

}