package com.immranderson.eazeinterview.android.holders.view_holders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.immranderson.eazeinterview.android.databinding.HolderRecycler200dpBinding;
import com.immranderson.eazeinterview.android.holders.state_models.RecyclerStateModel;

public class RecyclerHolder extends BaseViewHolder<RecyclerStateModel> {

    private HolderRecycler200dpBinding b;

    public RecyclerHolder(View itemView) {
        super(itemView);
        b = DataBindingUtil.bind(itemView);
    }

    @Override
    public void bindData(RecyclerStateModel recyclerStateModel) {

        b.recyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
        b.recyclerView.setAdapter(recyclerStateModel.baseRecyclerAdapter);

    }

}
