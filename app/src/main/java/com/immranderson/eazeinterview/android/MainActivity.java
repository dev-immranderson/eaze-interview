package com.immranderson.eazeinterview.android;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.immranderson.eazeinterview.android.adapters.BaseRecyclerAdapter;
import com.immranderson.eazeinterview.android.databinding.ActivityMainBinding;
import com.immranderson.eazeinterview.android.holders.state_models.MainGifCardHolderState;
import com.immranderson.eazeinterview.android.holders.state_models.MainHeaderState;
import com.immranderson.eazeinterview.android.holders.state_models.RecyclerStateModel;
import com.immranderson.eazeinterview.android.models.GifModel;
import com.immranderson.eazeinterview.android.network.RestAdapter;
import com.immranderson.eazeinterview.android.network.SearchGiphyApi;
import com.immranderson.eazeinterview.android.network.TrendingGiphyApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import retrofit2.Response;

/**
 * This acts as sort of a homepage where trending gifs and potentially popular search terms are initially displayed.
 * Doing subsequent searches inserts items at the top of the list.
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ActivityMainBinding b;
    private BaseRecyclerAdapter mAdapter;
    private Gson gson = new Gson();

    private List<String> homePageSearchTerms = Arrays.asList("Eaze", "Dog", "Cat", "Sheep", "Racoon", "Frogs", "Hello World");
    private List<Observable<Wrapper>> searchObservableWrappers = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        b = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mAdapter = new BaseRecyclerAdapter();


        //First, we build a list of observables that we'll want to execute concurrently, starting with trending, then with our other predefined terms
        Observable<Wrapper> trendingWrapper = RestAdapter.getInstance().create(TrendingGiphyApi.class)
                .getTrendingGifs(BuildConfig.GIPHY_API)
                .map(trendingResponse -> new Wrapper("Trending", trendingResponse))
                .observeOn(AndroidSchedulers.mainThread());

        searchObservableWrappers.add(trendingWrapper);

        for (String string : homePageSearchTerms) {
            searchObservableWrappers.add(generateSearchObservableWrapper(string));
        }


        /**
         * Observable.zip will allow us to execute our network requests in parallel. It will block displaying the content until all requests have finished executing.
         * Since these requests are done concurrently, order of completion isn't guaranteed. As a result, a wrapper object must be created so we can pass
         * the search term down the observable chain.
         */
        Observable.zip(searchObservableWrappers, response -> response)
                .subscribe(zipResponse -> {

                    for (Object o : zipResponse) {

                        Wrapper individualSearchResponse = (Wrapper) o;

                        //TODO: Account for Response isSuccessful()
                        JsonObject responseBody = individualSearchResponse.responseJson.body();

                        JsonArray data = responseBody.get("data").getAsJsonArray();
                        BaseRecyclerAdapter subGifRecyclerAdapter = new BaseRecyclerAdapter();

                        for (int i = 0; i < data.size(); i++) {

                            GifModel gifModel = gson.fromJson(data.get(i), GifModel.class);
                            subGifRecyclerAdapter.addViewData(new MainGifCardHolderState(gifModel));

                        }

                        if (individualSearchResponse.string.equals("Trending")) {

                            mAdapter.addViewDataAtPosition(0, new MainHeaderState("Trending"));
                            mAdapter.addViewDataAtPosition(1, new RecyclerStateModel(subGifRecyclerAdapter));

                        } else {

                            mAdapter.addViewData(new MainHeaderState(individualSearchResponse.string));
                            mAdapter.addViewData(new RecyclerStateModel(subGifRecyclerAdapter));

                        }


                    }

                    b.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                    b.recyclerView.setAdapter(mAdapter);

                }, error -> {

                    //TODO: Error handling -- network connectivity, etc...
                    error.printStackTrace();

                }, () -> {


                });


        //TODO: Add a spinner to inform user that network request is executing
        //Tapping search first animates in a header into the recyclerview, then animates insertion of the subsequent gifs after the network request is executed
        b.searchButton.setOnClickListener(v -> {

            String searchTerm = b.searchEditText.getText().toString();

            b.searchEditText.clearFocus();

            if (!TextUtils.isEmpty(searchTerm)) {
                mAdapter.addViewDataAtPosition(0, new MainHeaderState(searchTerm));
                mAdapter.notifyItemInserted(0);
                b.recyclerView.scrollToPosition(0);

                generateSearchObservableWrapper(searchTerm)
                        .subscribe(response -> {

                            JsonArray jsonArray = response.responseJson.body().get("data").getAsJsonArray();
                            BaseRecyclerAdapter subGifRecyclerAdapter = new BaseRecyclerAdapter();

                            for (int i = 0; i < jsonArray.size(); i++) {

                                GifModel gifModel = gson.fromJson(jsonArray.get(i), GifModel.class);
                                subGifRecyclerAdapter.addViewData(new MainGifCardHolderState(gifModel));

                            }

                            mAdapter.addViewDataAtPosition(1, new RecyclerStateModel(subGifRecyclerAdapter));
                            mAdapter.notifyItemInserted(1);

                        });
            }

        });

    }


    /**
     * Generate wrapper object for our search observables so we can have access to the search text.
     */
    private Observable<Wrapper> generateSearchObservableWrapper(String searchText) {

        return RestAdapter.getInstance()
                .create(SearchGiphyApi.class)
                .searchGiphy(BuildConfig.GIPHY_API, searchText)
                .map(result -> new Wrapper(searchText, result))
                .observeOn(AndroidSchedulers.mainThread());

    }

    private class Wrapper {

        public String string;
        public Response<JsonObject> responseJson;

        public Wrapper(String string, Response<JsonObject> responseJson) {

            this.string = string;
            this.responseJson = responseJson;
        }

    }

}
