package com.immranderson.eazeinterview.android.adapters;

/**
 * Concrete implementation of @AbstractBaseRecyclerAdapter. If you want more customization within your adapter
 * you can just choose to extend from @AbstractBaseRecyclerAdapter, but in 99% of cases this concrete implementation
 * will work just fine.
 */
public class BaseRecyclerAdapter extends AbstractBaseRecyclerAdapter {

}