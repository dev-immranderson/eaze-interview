package com.immranderson.eazeinterview.android.holders.state_models;

import android.view.ViewGroup;

import com.immranderson.eazeinterview.android.R;
import com.immranderson.eazeinterview.android.holders.HolderTypes;
import com.immranderson.eazeinterview.android.holders.view_holders.BaseViewHolder;
import com.immranderson.eazeinterview.android.holders.view_holders.MainHeaderHolder;

public class MainHeaderState extends BaseStateModel {

    public final String headerText;

    public MainHeaderState(String headerText) {
        this.headerText = headerText;
    }

    @Override
    public int getType() {
        return HolderTypes.MAIN_HEADER;
    }

    @Override
    public int getViewHolderResId() {
        return R.layout.holder_header_text;
    }

    @Override
    public BaseViewHolder createViewHolder(ViewGroup parent) {
        return new MainHeaderHolder(inflateFrom(parent));
    }
}
