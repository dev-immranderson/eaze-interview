package com.immranderson.eazeinterview.android.network;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchGiphyApi {

//    https://api.giphy.com/v1/gifs/search?api_key=b15a7aa4a9f54cb1a7389143c7eaca18&q=cat&limit=25&offset=0&rating=G&lang=en

    @GET("search")
    Observable<Response<JsonObject>> searchGiphy(@Query("api_key") String api_key, @Query("q") String query);
}
