package com.immranderson.eazeinterview.android.holders.view_holders;

import android.databinding.DataBindingUtil;
import android.view.View;

import com.bumptech.glide.Glide;
import com.immranderson.eazeinterview.android.databinding.HolderSearchGifImageBinding;
import com.immranderson.eazeinterview.android.holders.state_models.SearchGifCardHolderState;

public class SearchGifCardHolder extends BaseViewHolder<SearchGifCardHolderState> {

    private final HolderSearchGifImageBinding b;

    public SearchGifCardHolder(View itemView) {
        super(itemView);
        b = DataBindingUtil.bind(itemView);
    }

    @Override
    public void bindData(SearchGifCardHolderState searchGifCardHolderState) {
        Glide.with(itemView.getContext())
                .asGif()
                .load(searchGifCardHolderState.gifModel.images.gifDownsized.url)
                .into(b.gifImageView);
    }

}
