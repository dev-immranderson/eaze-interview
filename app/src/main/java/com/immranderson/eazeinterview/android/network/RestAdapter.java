package com.immranderson.eazeinterview.android.network;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAdapter {

    private static Retrofit RETROFIT_INSTANCE;
    private static OkHttpClient OK_HTTP_CLIENT_INSTANCE;

    public static Retrofit getInstance() {

        if (RETROFIT_INSTANCE == null) {

            RETROFIT_INSTANCE = new Retrofit.Builder()
                    .baseUrl("https://api.giphy.com/v1/gifs/")
                    .client(getOkHttpInstance())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setLenient().create()))
                    .build();

        }

        return RETROFIT_INSTANCE;

    }

    private static OkHttpClient getOkHttpInstance() {

        if (OK_HTTP_CLIENT_INSTANCE == null) {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(15, TimeUnit.SECONDS);
            builder.writeTimeout(15, TimeUnit.SECONDS);
            builder.retryOnConnectionFailure(true);

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);


            OK_HTTP_CLIENT_INSTANCE = builder.build();

        }

        return OK_HTTP_CLIENT_INSTANCE;

    }

}
