package com.immranderson.eazeinterview.android.holders.state_models;

import android.view.ViewGroup;

import com.immranderson.eazeinterview.android.R;
import com.immranderson.eazeinterview.android.models.GifModel;
import com.immranderson.eazeinterview.android.holders.HolderTypes;
import com.immranderson.eazeinterview.android.holders.view_holders.BaseViewHolder;
import com.immranderson.eazeinterview.android.holders.view_holders.SearchGifCardHolder;

public class SearchGifCardHolderState extends BaseStateModel {

    public GifModel gifModel;

    public SearchGifCardHolderState(GifModel gifModel) {
        this.gifModel = gifModel;
    }

    @Override
    public int getType() {
        return HolderTypes.GIF_CARD;
    }

    @Override
    public int getViewHolderResId() {
        return R.layout.holder_search_gif_image;
    }

    @Override
    public BaseViewHolder createViewHolder(ViewGroup parent) {
        return new SearchGifCardHolder(inflateFrom(parent));
    }
}
