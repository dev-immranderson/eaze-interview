package com.immranderson.eazeinterview.android.holders.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.immranderson.eazeinterview.android.holders.state_models.BaseStateModel;


public abstract class BaseViewHolder<T extends BaseStateModel> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindData(T t);

}